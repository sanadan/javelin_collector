class MediaController < ApplicationController
  before_action :authenticate_user!
  before_action :set_medium, only: %i[show edit update destroy]

  # GET /media or /media.json
  def index
    @tag_name = params[:tag]
    tag = Tag.find_by(name: @tag_name)
    media = tag&.media || Medium.all
    @pagy, @media = pagy(media.where(user: current_user))
  end

  # GET /media/1 or /media/1.json
  def show
  end

  # GET /media/new
  def new
    @medium = Medium.new
  end

  # GET /media/1/edit
  def edit
  end

  # POST /media or /media.json
  def create
    @medium = Medium.new(medium_params)
    @medium.user_id = current_user.id
    tag_names = params[:medium][:tag_names].split
    @medium.save_tags(tag_names)

    respond_to do |format|
      if @medium.save
        format.html { redirect_to medium_url(@medium), notice: t('.created') }
        format.json { render :show, status: :created, location: @medium }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /media/1 or /media/1.json
  def update
    tag_names = params[:medium][:tag_names].split
    @medium.save_tags(tag_names)

    respond_to do |format|
      from = @medium.file.path
      thumbnail_from = @medium.file.thumb.path
      if @medium.update(medium_params)
        @medium = Medium.find(@medium.id) # 読み直さないと新しいパスにならない
        @medium.move_file(from, thumbnail_from)
        format.html { redirect_to medium_url(@medium), notice: t('.updated') }
        format.json { render :show, status: :ok, location: @medium }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @medium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /media/1 or /media/1.json
  def destroy
    @medium.destroy

    respond_to do |format|
      format.html { redirect_to media_url, notice: t('.deleted') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_medium
    @medium = Medium.find(params[:id])
    @medium.tag_names = []
    @medium.tags.each do |tag|
      @medium.tag_names << tag.name
    end
  end

  # Only allow a list of trusted parameters through.
  def medium_params
    params.require(:medium).permit(:name, :author, :file, :file_cache)
  end
end
