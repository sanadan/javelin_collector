class MyPageController < ApplicationController
  before_action :authenticate_user!

  def index
    @tag_name = params[:tag]
    tag = Tag.find_by(name: @tag_name)
    media = tag&.media || Medium.all
    @pagy, @media = pagy(media.where(user: current_user))
  end
end
