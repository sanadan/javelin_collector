class Medium < ApplicationRecord
  belongs_to :user
  has_many :medium_tags, dependent: :destroy
  has_many :tags, through: :medium_tags

  mount_uploader :file, MediaUploader

  attr_accessor :tag_names

  validates :name, presence: true

  before_save :normalize

  def save_tags(tag_names)
    MediumTag.where(medium_id: id).destroy_all if tags.present?

    tag_names.each do |tag|
      inspected_tag = Tag.where(name: tag).first_or_create
      tags << inspected_tag
    end
  end

  def move_file(from, thumbnail_from)
    to = file.path
    thumbnail_to = file.thumb.path
    return if from == to

    move_file_sub(from, to)
    move_file_sub(thumbnail_from, thumbnail_to)
  end

  private

  def move_file_sub(from, to)
    FileUtils.mkdir_p(File.dirname(to))
    FileUtils.mv(from, to)
    FileUtils.rm_r(File.dirname(from), secure: true)
    begin
      FileUtils.rmdir(File.dirname(from, 2))
    rescue Errno::ENOTEMPTY
      # 削除できなければ無視して続行
    end
  end

  def normalize
    author.tr!('\\', '＼')
    author.tr!('/', '／')
    author.tr!(':', '：')
    author.tr!('*', '＊')
    author.tr!('?', '？')
    author.tr!('<', '＜')
    author.tr!('>', '＞')
    author.tr!('|', '｜')
  end
end
