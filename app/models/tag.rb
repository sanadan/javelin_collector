class Tag < ApplicationRecord
  has_many :medium_tags, dependent: :destroy
  has_many :media, through: :medium_tags

  validates :name, presence: true
end
