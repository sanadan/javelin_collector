class CreateMedia < ActiveRecord::Migration[7.0]
  def change
    create_table :media do |t|
      t.references :user, null: false, index: true
      t.string :name, null: false, index: true
      t.string :author, null: false, index: true
      t.string :file

      t.timestamps
    end
  end
end
