class CreateMediumTags < ActiveRecord::Migration[7.0]
  def change
    create_table :medium_tags do |t|
      t.references :medium, null: false, foreign_key: true
      t.references :tag, null: false, foreign_key: true

      t.timestamps
    end
  end
end
