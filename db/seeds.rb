# rubocop:disable Rails/Output

if User.first.nil?
  puts 'Create default user'
  User.create!(email: 'user@example.com', password: 'password')
end

# rubocop:enable Rails/Output
