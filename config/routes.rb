Rails.application.routes.draw do
  resources :media

  get 'my_page', to: 'my_page#index'

  devise_for :users, controllers: { sessions: 'sessions' }

  get 'home', to: 'home#index'

  root 'home#index'
end
